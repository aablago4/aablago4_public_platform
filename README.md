# aablago4_public_platform

This awesome project allows you to deploy your hipster shop project with only few clicks and manual input.

After successful deploy you will get:
1. Managed k8s installation at YandexCloud
2. Harbor as artifacts store
3. Ingress nginx with metrics as an ingress controller
4. Prometheus and Grafana for monitoring
5. Loki for collecting logs
6. Certbot for letsencrypt certificates auto-issuing and rearming
7. Hashicorp vault with autounseal option, high availability and web interface
8. Your hipster-shop microservices application
9. All infrastrucure components and application will have published web endpoint secured wit letsencrypt certificate.

## Prepare Gitlab
1. Get Gitlab personal access token with admin privileges [info](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)

## Prepare YandexCloud
1. Create new or get service account at YandexCloud with admin privileges.
2. Create AUTH-key for service account (https://cloud.yandex.ru/docs/iam/operations/authorized-key/create)
3. Download JSON keyfile and upload it to gitlab secure files to project *Settings-CI/CD-Secure Files*

## Prepare repo
1. There is two ways to set sensitive variables.
   1. Secure way. Set project variables. *Settings-CI/CD-Variables*
   2. Insecure way. In the top of gitlab-ci.yml file. Just uncomment neccesary lines at the top of file.
2. List of sensitive variables
   1. YC_CLOUD_ID - [YandexCloud Cloud ID](https://cloud.yandex.com/en-ru/docs/resource-manager/operations/cloud/get-id)
   2. YC_CLOUDFOLDER_ID - [YandexCloud Folder ID](https://cloud.yandex.com/en-ru/docs/resource-manager/operations/folder/get-id)
   3. YC_KEY_NAME - json keyfile name
   4. GITLAB_TOKEN - Gitlab personal access token with admin privileges [info](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
3. List of optional variables
   1. YC_K8S_NODEGROUP_ID - [info](https://cloud.yandex.ru/docs/compute/concepts/vm-platforms)
   2. YC_K8S_NODEGROUP_CPUS - compute performance params [info](https://cloud.yandex.ru/docs/compute/concepts/performance-levels)
   3. YC_K8S_NODEGROUP_RAM - compute performance params [info](https://cloud.yandex.ru/docs/compute/concepts/performance-levels)
   4. YC_K8S_NODEGROUP_SIZE - compute nodes count
   5. YC_ZONE - networking zone for worker nodes
   6. CERTBOT_ENV - option for letsencrypt certificates. default is "staging". https://letsencrypt.org/docs/staging-environment/
4. If this is first launch make sure to delete these files:
   1. [Old ingresses](/cluster/deploy/manifests/ingresses.yaml)
   2. [Old vault specific release](/cluster/deploy/releases/vault.yaml)

## Post Tasks
After successful deployment you need to initialize vault cluster and save tokens.
For this you need to:
1. Find pod vault-0 and enter it's shell: kubectl exec -it vault-0 -n=vault -- bash
2. Initialize vault using command: vault operator init
3. Save all output. Root Token is IMPORTANT and will be used for the first login
4. Go to other vault node's shell (vault-1, vault-2)
5. Join both nodes to raft cluster: vault operator raft join http://vault-0.vault-internal:8200

## Jobs description
1. Plan
   1. Job generates terraform playbook variables using project variables
   2. Job downloads secure keyfile that will be used with terraform
   3. Terraform plans deployment and save state and cache
2. Deploy
   1. Job generates terraform playbook variables using project variables
   2. Job downloads secure keyfile that will be used with terraform
   3. Terraform makes deployment and save state and cache
3. Bootstrap cluster
   1. Job downloads secure keyfile, kubectl, flux, yc cli installations and prepares environment
   2. Job bootsraps fluxcd to new k8s cluster built at previous steps
   3. After bootstraping flux starts to deploy manifests at the cluster folder in the root of the repo
4. Flux
   1. Repository contains manifests to deploy infrastructure components and application
   2. Repository contains policies for flux to scan repo for new container images and automatically pushes it to cluster
5. Bootsrap Ingresses
   1. Job downloads secure keyfile, kubectl, yc cli installations and prepares environment
   2. Job generates manifests for ingresses with external ip and ssl-certificate based on environment
   3. Job generates vault release manifest for auto unseal procedure
   4. Job pushes new manifests to repo
   5. Flux automatically deploys new manifests to cluster
6. Destroy
   1. Job generates terraform playbook variables using project variables
   2. Job downloads secure keyfile that will be used with terraform
   3. Terraform plans and destroys k8s managed cluster