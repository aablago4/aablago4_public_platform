terraform {
  required_providers {
    yandex = {
      source  = "yandex"
      version = "0.97.0"
    }
  }
}

provider "yandex" {
  service_account_key_file = var.yandex_cloud.keyfile   # Set OAuth or IAM token
  cloud_id                 = var.yandex_cloud.cloud_id  # Set your cloud ID
  folder_id                = var.yandex_cloud.folder_id # Set your cloud folder ID
  zone                     = var.yandex_cloud.zone      # Availability zone by default, one of ru-central1-a, ru-central1-b, ru-central1-c
}

