variable "yandex_cloud" {
  description = "Yandex cloud account params"
  type = object({
    keyfile   = string
    cloud_id  = string
    folder_id = string
    zone      = string
  })
  default = {
    keyfile   = "YC_KEY_NAME"
    cloud_id  = "YC_CLOUD_ID"
    folder_id = "YC_CLOUDFOLDER_ID"
    zone      = "YC_ZONE"
  }
}

variable "nodegroup" {
  type = object({
    platform_id = string
    cores       = string
    memory      = string
    size        = number
  })
  default = {
    platform_id = "YC_K8S_NODEGROUP_ID"
    cores       = "YC_K8S_NODEGROUP_CPUS"
    memory      = "YC_K8S_NODEGROUP_RAM"
    size        = "YC_K8S_NODEGROUP_SIZE"
  }
}